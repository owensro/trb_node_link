//////////////
/////////
//////
// Author: Rob Owens
// main.js
//////
/////////
//////////////

// https://bl.ocks.org/mbostock/4600693
var finalData = {}
var nodeChart
var existsSelectedNode = false

// Read the data
// d3.csv(google_form_link).then(function(data) {
d3.csv("data/trb_link_data.csv").then(function(data) {

    var nodes = []
    var links = []

    data.forEach(function(d) {
        let committee = d["Committee Name"]
        let website = d["Committee’s website URL"]
        let shortTerm = d["Short term (1-3 year) Goals"]
        let vision = d["One sentence Vision Statement (Simply communicate your committee's goal)"]

        // Nodes
        d["topics"] = d["Topics "].split(';')
        d["criticalIssues"] = d["Critical Issues (Select all that apply)"].split(';')
        d["trends"] = d["Trends/Hot Topics"].split(';')

        d["topics"].forEach(function(d) {
            size = getRandomIntInclusive(minSize, maxSize * 3)
            nodes.push( {id: d, type: "topics", isSelected: false,
                         isNeighbor: false, radius: size, isCategory: true,
                         color1: orange1, color2: orange2, color3: orange3} )
            links.push( { source: committee, target: d } )
        })

        d["criticalIssues"].forEach(function(d) {
            size = getRandomIntInclusive(minSize, maxSize * 3)
            nodes.push( {id: d, type: "criticalIssues", isSelected: false,
                         isNeighbor: false, radius: size, isCategory: true,
                         color1: blue1, color2: blue2, color3: blue3} )
            links.push( { source: committee, target: d } )
        })

        d["trends"].forEach(function(d) {
            size = getRandomIntInclusive(minSize, maxSize * 3)
            nodes.push( {id: d, type: "trend", isSelected: false,
                         isNeighbor: false, radius: size, isCategory: true,
                         color1: green1, color2: green2, color3: green3} )
            links.push( { source: committee, target: d } )
        })
        size = getRandomIntInclusive(minSize, maxSize * 3)

        nodes.push( {id: committee, type: "committee", isSelected: false,
                     isNeighbor: false, radius: size, isConnectedCommittee: false,
                     vision : vision, shortTerm: shortTerm, website : website,
                     color1: purple1, color2: purple2, color3: purple3} )
    })

    nodes = removeDuplicates(nodes, "id")

    finalData = { nodes: nodes, links: links }

    // Create drop down of Committees
    var selector = d3.select("#committeeSelect")
        .selectAll("options")
        .data(finalData.nodes.filter(function(d){
            return d.type == "committee"
        }))
        .enter().append("option")
        .text(function(d) {
            return d.id;
        })
        .attr("value", function (d) {
            return d.id;
        });

    nodeChart = new NodeLinkChart("#chart-area")
    legend = new Legend("#legend")
})
