//////////////
/////////
//////
// Author: Rob Owens
// NodeLinkChart.js
//////
/////////
//////////////

Legend = function(_parentElement) {
    this.parentElement = _parentElement;
    this.initLegend();
}

Legend.prototype.initLegend = function() {

    var vis = this;

    vis.height = 100;
    vis.width = 200;

    vis.svg = d3.select(vis.parentElement)
        .append("svg")
        .attr("width", vis.width)
        .attr("height", vis.height)

    // Legend
    const legendKey = [
        Committees = { name: "Committees", color: purple1 },
        Topics = {  name: "Topics", color: orange1 },
        Trends = {  name: "Trends/Hot Topics", color: green1 },
        criticalIssues = {  name: "Critical Issues", color: blue1 }
    ]

    vis.svg.selectAll("LegendTitle")
        .append("text")
        .text("Legend")
        .attr("text-anchor", "center")
        .style("alignment-baseline", "middle")
        .attr("x", 50)
        .attr("y", 0)

    vis.svg.selectAll("legendDots")
      .data(legendKey)
      .enter()
      .append("circle")
        .attr("class", "nodeClass")
        .attr("cx", 10)
        .attr("cy", function(d,i) { return 10 + i * 25 }) // 100 is where the first dot appears. 25 is the distance between dots
        .attr("r", 7)
        .style("fill", function(d) {
            console.log(d)
            return d.color
        })

    vis.svg.selectAll("legendLabels")
      .data(legendKey)
      .enter()
      .append("text")
        .attr("x", 30)
        .attr("y", function(d,i){ return 10 + i*25}) // 100 is where the first dot appears. 25 is the distance between dots
        .style("fill", function(d){ return d.color })
        .text(function(d){ return d.name })
        .attr("text-anchor", "left")
        .style("alignment-baseline", "middle")
}

NodeLinkChart = function(_parentElement) {
    this.parentElement = _parentElement;
    this.initVis();
}

/////////////////////
// **  initVis  ** //
/////////////////////
NodeLinkChart.prototype.initVis = function() {
    var vis = this

    vis.height = 800;
    vis.width = 1300

    vis.svg = d3.select(vis.parentElement)
        .append("svg")
        .attr("width", vis.width)
        .attr("height", vis.height)
        .on("dblclick", function() {
            existsSelectedNode = false
            closeNav();
            $('#cat-select').hide()
            $('#btn-div').hide()
            finalData.nodes.forEach(function(d){
                size = getRandomIntInclusive(minSize, maxSize * 3)
                d.isNeighbor = false
                d.isSelected = false
                d.isConnectedCommittee = false
                d.radius = size
            })
            vis.wrangleData()
        });

    // Initial Rendering to ensure links are behind the nodes
    vis.svg.append("g").attr("id", "links")
    vis.svg.append("g").attr("id", "nodes")

    vis.g = vis.svg.append("g")
    vis.t = function() { return d3.transition().duration(1000); }

    vis.innerCircleSimulation = d3.forceSimulation()
        .force("link", d3.forceLink().strength(0).id(function(d) {
            return d.id;
        }))
        .force("collide", d3.forceCollide().radius(function(d) {
            return d.radius * 1.5
        }))
        .force("y", d3.forceY().y(function(d) {
            if (existsSelectedNode & d.isSelected) {
                return 300;
            } else {
                return 400;
            }
        }).strength(0.05))
        .force("x", d3.forceX().x(function(d) {
            if (existsSelectedNode) {
                if (d.type == "committee" & d.isConnectedCommittee) {
                    return 850;
                } else if (d.type == "committee") {
                    return 575;
                } else {
                    return 400;
                }
            } else {
                return 600
            }
        }).strength(function(){
            if (existsSelectedNode) {
                return 1
            } else {
                return 0.05
            }
        }))

    // How to get this centered??????????? AHHHH!
    vis.outerCircleSimulation = d3.forceSimulation()
        .force("charge", d3.forceCollide().radius(function(d){
            return d.radius;
        }))
        .force("radius", d3.forceRadial(550).x(600).y(400))

    vis.node = vis.svg.select("#nodes").selectAll(".nodes")
        .data(finalData.nodes)
        .enter()
            .append("g")
            .attr("class", "nodes")

    vis.node.append("circle")
        .on("mouseover", function(d) {
            vis.svg.selectAll("#clickedNeighborsText").remove()
            nodeHovered(d, vis);
        })
        .on("mouseout", function(d){
            nodeUnhovered(d, vis);
            addNodeLabels(vis)
        })
        .on("click", function(d) {
            nodeClicked(d, vis);
        })

    vis.innerCircleSimulation
        .nodes(finalData.nodes)
        .force("link").links(finalData.links)

    vis.outerCircleSimulation
        .nodes(finalData.nodes)

    vis.wrangleData();
}

/////////////////////////
// **  wrangleData  ** //
/////////////////////////
NodeLinkChart.prototype.wrangleData = function(){

    var vis = this

    vis.selectedCat = $("#categorySelect").val()
    switch (vis.selectedCat) {
        case "Topics ":
            vis.selectedCat = "topics";
            break;
        case "Critical Issues":
            vis.selectedCat = "criticalIssues";
            break;
        case "Trends/Hot Topics":
            vis.selectedCat = "trend";
            break;
    };

    vis.innerCircleNodes = finalData.nodes.filter(function(d) {
        return (
            // Remove "is neighbor connected to currently selected node"
            (d.isNeighbor && d.type == vis.selectedCat || d.isConnectedCommittee || d.isSelected) ||
            (!existsSelectedNode)
        );
    })

    vis.nodeNames = []
    vis.innerCircleNodes.forEach(function(d) {
            vis.nodeNames.push(d.id)
    })

    vis.outerCircleNodes = finalData.nodes.filter(function(d){
        return !vis.nodeNames.includes(d.id)
    })

    vis.filteredLinks = finalData.links.filter(function(d) {
        return (
            (vis.nodeNames.includes(d.target.id) &&
             d.target.type == vis.selectedCat &&
             !d.source.isSelected) || !existsSelectedNode
        );

    });

    vis.updateChart();

}

/////////////////////////
// **  updateChart  ** //
/////////////////////////
NodeLinkChart.prototype.updateChart = function() {
    // https://bl.ocks.org/mbostock/1095795
    var vis = this

    vis.svg.select("#highlight_committee").remove()
    vis.svg.select("#committeeText").remove()
    vis.svg.select("#categoryText").remove()
    vis.svg.select("#helperText").remove()
    vis.svg.select("#helperText2").remove()

    // Selected Committee
    vis.svg.append("text")
        .attr("id", "committeeText")
        .attr("x", 625)
        .attr("y", 75)
        .attr("font-size", 32)
        .attr("font-weight", "bold")
        .attr("fill", "black")
        .attr("font-style", "italic")
        .attr("opacity", "0.7")
        .attr("text-anchor", "middle")
        .text(function() {
            if (existsSelectedNode) {
                return $("#committeeSelect").val()
            }
        })

    // Topics Connected
    vis.svg.append("text")
        .attr("id", "categoryText")
        .attr("x", 250)
        .attr("y", 400)
        .attr("font-size", 16)
        .attr("fill", "gray")
        .attr("font-style", "italic")
        .attr("opacity", "0.7")
        .attr("text-anchor", "middle")
        .text(function() {
            if (existsSelectedNode) {
                return $("#categorySelect").val()
            }
        });

    // Connected Committees
    vis.svg.append("text")
        .attr("id", "helperText")
        .attr("x", 850)
        .attr("y", 175)
        .attr("dy", "0em")
        .attr("font-size", 16)
        .attr("fill", "gray")
        .attr("font-style", "italic")
        .attr("opacity", "0.7")
        .attr("text-anchor", "middle")
        .text(function() {
            if (existsSelectedNode) {
                return "Connected "
            }
        });

    // Connected Committees
    vis.svg.append("text")
        .attr("id", "helperText2")
        .attr("x", 850)
        .attr("y", 175)
        .attr("dy", "1em")
        .attr("font-size", 16)
        .attr("fill", "gray")
        .attr("font-style", "italic")
        .attr("opacity", "0.7")
        .attr("text-anchor", "middle")
        .text(function() {
            if (existsSelectedNode) {
                return "Committees"
            }
        });

    d3.selectAll("circle")
       .transition(vis.t)
       .attr("r", function(d) {
           if (d.radius > 0) {
               return d.radius
           } else {
               return 7
           }
       })
       .attr('fill', function(d) {
           if (d.isNeighbor & d.type == "committee" | d.type == vis.selectedCat) {
               return d.color2
           } else {
               return d.color1
           }
       })

   vis.link = vis.svg.select("#links").selectAll(".link")
       .data(vis.filteredLinks, function(d) {
           return d.source.id + d.target.id;
       })
       .join(
          enter => enter.append("line")
                   .attr("class", "link")
                   .attr("stroke-width", 1)
                   .attr("stroke-opacity", 0.5)
                   .attr("stroke", "gray"),
          update => update.attr("stroke", "gray")
          )

    addNodeLabels(vis)

    vis.innerCircleSimulation
        .nodes(vis.innerCircleNodes)
        .on("tick", ticked)
        .alpha(1)
        .restart()

    vis.outerCircleSimulation
        .nodes(vis.outerCircleNodes)
        .on("tick", ticked)
        .alpha(1)
        .restart();

    vis.innerCircleSimulation.force("link").links(vis.filteredLinks)

    function ticked() {
        vis.link
          .attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });

        vis.node.attr("transform", function(d) {
            return "translate(" + d.x + "," + d.y + ")";
        })
    }

}
