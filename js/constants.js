//////////////
/////////
//////
// Author: Rob Owens
// Constants.js
//////
/////////
//////////////

// const google_form_link = "https://docs.google.com/spreadsheets/d/1YH5LWPcxDp2NryHd2-6nbINY9FDUOnS6vk359klxK5g/export?format=csv"
// const google_form_link = "https://docs.google.com/spreadsheets/d/1FuTURB7bFsgKCVDtCqhUSZEf2xs3ZNmebNc86Lcwk7o/export?format=csv"
// const google_form_link = "https://docs.google.com/spreadsheets/d/1n5bq4L6DKR9eulf6Kzv51AfHLUXmeTfjzyS5qg_bqIU/export?format=csv"
const color = d3.scaleOrdinal(d3.schemeCategory10);
const minSize = 8
const maxSize = 35

// Colors
const purple1 = "#E347FC"
const orange1 = "#FC8847"
const green1 = "#61FC47"
const blue1 = "#47BBFC"

const purple2 = "#CD03EE"
const orange2 = "#EE5703"
const green2 = "#24EE03"
const blue2 = "#039AEE"

const purple3 = "#A603C1"
const orange3 = "#C14703"
const green3 = "#1DC103"
const blue3 = "#037DC1"
