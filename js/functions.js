//////////////
/////////
//////
// Author: Rob Owens
// Functions.js
//////
/////////
//////////////

function nodeClicked(clickedNode, vis) {

    highlightNeighbors(clickedNode, vis);

}

function removeDuplicates(originalArray, prop) {

     var newArray = [];
     var lookupObject  = {};

     for(var i in originalArray) {
        lookupObject[originalArray[i][prop]] = originalArray[i];
     }

     for(i in lookupObject) {
         newArray.push(lookupObject[i]);
     }
      return newArray;

 };

function getNeighbors(node) {

    // If the user selects on a committee
    if (node.type == "committee") {
        return finalData.links.reduce((neighbors, link) => {
            if (link.source === node.id || link.source.id === node.id) {
                neighbors.push(link.target) // The topic selected
            }
            return neighbors
        }, [node.id])
    }

}

function isNeighborLink(node, link) {
    return link.target.id === node.id || link.source.id === node.id
}

// Actions //
function highlightNeighbors(selectedNode, vis) {

    var committeeInception = false
    // if (existsSelectedNode && selectedNode.type == "committee") {
    //     committeeInception = true
    //     nodeChart.wrangleData()
    //     return
    // }

    if (selectedNode.type == "committee") {
        $("#committeeSelect").val(selectedNode.id);
        $('#com_name').text(selectedNode.id);
        $('#committee-website').attr("href", selectedNode.website);
        $('#vision').text(selectedNode.vision);
        $('#short-goals').text(selectedNode.shortTerm);
        $('#cat-select').show()
        $('#btn-div').show()
    }

    finalData.nodes.forEach(function(d) {
        d.isConnectedCommittee = false
        d.connectedTypeNode = false
    })

    existsSelectedNode = true
    const selectedNodeNeighbors = getNeighbors(selectedNode)
    const notSelectedCommittees = finalData.nodes.filter(function(d) {
        return d.type == "committee" && d.id != selectedNode.id
    })

    // Connected Committees
    finalData.nodes.filter(function(d) {
        return d.type == "committee" && d.id != selectedNode.id
    }).forEach(function(d) {
            d.numberConnections = 0
            const possibleNeighbors = getNeighbors(d)
            selectedNodeNeighbors.forEach(function(i) {
                if (possibleNeighbors.includes(i)) {
                    i.connectedTypeNode = true
                    d.isConnectedCommittee = true
                    d.numberConnections += 1
                }
            })
        })

    finalData.nodes.forEach(function(d) {

        size = getRandomIntInclusive(minSize, maxSize)

        if (d.type == "committee") {
            d.radius = Math.max(maxSize, d.numberConnections * 2)s
        } else {
            d.radius = size
        }

        d.isNeighbor = false
        d.isSelected = false

        selectedNodeNeighbors.forEach(function(j) {
            size = getRandomIntInclusive(minSize + 5, maxSize)
            if (j == d.id || j.id == d.id) {
                d.isNeighbor = true
                d.radius = size
            }
        })

    });

     selectedNode.isSelected = true
     selectedNode.radius = 1
     nodeChart.wrangleData()

 };

function nodeHovered(node, vis) {

    if (!existsSelectedNode ||
        (node.type == "committee" || node.type == vis.selectedCat)) {

            linkIds = []
            var links = vis.filteredLinks.forEach(function(d) {
                if (node.id == d.source.id | node.id == d.target.id) {
                    linkIds.push(d.target.id)
                    linkIds.push(d.source.id)
                }
            })

            d3.selectAll("circle")
                .transition().duration(500)
                .attr("fill", function(d){
                    if (linkIds.includes(d.id)) {
                        return d.color3;
                    } else {
                        if (existsSelectedNode) {
                            return d.color1
                        } else if (d.isNeighbor & d.type == "committee" | d.type == vis.selectedCat) {
                            return d.color2
                        } else {
                            return d.color1
                        }
                    }
                })
                .attr("opacity", function(d) {
                    if (linkIds.includes(d.id)) {
                        return 1
                    } else {
                        return 0.25
                    }
                });

            vis.svg.selectAll("line")
                .transition().duration(500)
                .style("stroke-opacity", function(d){
                    console.log(d);
                    return (
                        (d.target.id == node.id && d.source.isConnectedCommittee) |
                        (d.source.id == node.id && d.target.isNeighbor)
                    ) ? 1 : 0.1
                })

            vis.node.append("text")
                 .attr("id", "clickedNeighborsText")
                 .attr("class", "clickText")
                 .attr("font-size", 10)
                 .attr("dx", function(d) {
                     return "1em"
                 })
                 .attr("dy", function(d) {
                     return "2em"
                 })
                 .text(function(d) {
                     if (linkIds.includes(d.id)) {
                         return d.id
                     }
                 })

    }

};

 function nodeUnhovered(node, vis) {

     remove = vis.svg.selectAll("text").filter(function() {
         return this.id == "highlight_committee"
     })

     remove.remove()

     d3.selectAll("circle")
         .transition().duration(500)
         .attr("opacity", 1)
         .attr("fill", function(d){
             if (existsSelectedNode) {
                 return d.color1
             } else if (d.isNeighbor & d.type == "committee" | d.type == vis.selectedCat) {
                 return d.color2
             } else {
                 return d.color1
             }
         })

     d3.selectAll("line")
         .transition().duration(500)
         .style("stroke-opacity", 0.75)

 };

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive
}

function openNav() {
    document.getElementById('sidebar_panel').style.width = '400px';
    $('#sidebar_panel').css({'padding-left' : '20px',
                             'padding-right' : '20px'});
    document.getElementById('band').style.marginLeft = '450px';
}

/* Set the width of the sidebar to 0 and the left margin of the page content to 0 */
function closeNav()  {
    document.getElementById('sidebar_panel').style.width = '0';
    document.getElementById('band').style.marginLeft = 'auto';
    $('#sidebar_panel').css({'padding-left' : '0px',
                             'padding-right' : '0px'});
}

$("#committeeSelect").on("change", function(){

    const selectedNode = $("#committeeSelect").val()
    const node = finalData.nodes.filter(function(d){
        return d.id == selectedNode
    })

    highlightNeighbors(node[0]);
})

$("#categorySelect").on("change", function() {
    if (existsSelectedNode) {
        const selectedNode = $("#committeeSelect").val()
        const node = finalData.nodes.filter(function(d){
            return d.id == selectedNode
        })
        highlightNeighbors(node[0]);
    } else {
        nodeChart.wrangleData()
    }
})

function addNodeLabels(vis) {
    vis.svg.selectAll("#clickedNeighborsText").remove()

    vis.node.append("text")
         .attr("id", "clickedNeighborsText")
         .attr("class", "clickText")
         .attr("font-size", 10)
         .attr("dx", function(d) {
             return "1em"
         })
         .attr("dy", function(d) {
             return "2em"
         })
         .text(function(d) {
             if (existsSelectedNode) {
                 if (
                     (d.isNeighbor & d.type == vis.selectedCat) |
                     d.isConnectedCommittee
                 ) {
                     return d.id
                 }
             }
         })
}

// Open sidebar
$("#committee-btn").click(function(){
    openNav();
})

// Close sidebar
$("#close-btn").click(function(){
    closeNav();
})

// Close sidebar
$("#close-btn2").click(function(){
    $("#info-panel").hide()
})

// Close sidebar
$("#info-btn").click(function(){
    $("#info-panel").show()
})
